# Sample Django 4.2.2 application (courses4)

Sample Django 4.2.2 application

## Run application

To start Django server execute following command:
```shell
python manage.py runserver
```

To verify Django server open [http://127.0.0.1:8000/](http://127.0.0.1:8000/) in the browser.
This will show default page.

To access admin page open following URL: [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin). 
This will show an admin login page.

## Chapter 68: Database and Migrations in Django
### Run database migration

In order to make Django application usable database migration should be executed:

```shell
python manage.py migrate
```

This will apply default database migration, which will create required tables and other database objects.

### Create an admin user

To create an admin user execute following command:
```shell
python manage.py createsuperuser
```

Enter `admin` as _Username_, `admin@admin.com` as an _Email address_ and `admin` as a _Password_.
Accept user creation without password validation

Now it should be possible to access admin page. 
Open following URL in the browser [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin) and login with newly 
created admin user.

### Create and apply migration for the Shop application

In order to start using `shop` application's model migrations should be created with the following command:
```shell
python manage.py makemigrations
```

Migration file will be created in `shop/migrations` directory. The new migration should be applied with the following
command:
```shell
python manage.py migrate
```

### Create a Category using the Category model in the shell

It is possible to insert data into database from Django shell. To start the shell execute following command:
```shell
python manage.py shell
```

To insert a Category entry into database first need to import model, like this:
```python
from shop.models import Category
```

To get the data from database execute following command:
```python
from shop.models import Category
Category.objects.all()
```

To create and store new Category to database execute following command in Django shell:
```python
from shop.models import Category

new_category = Category(title='Programming')
new_category.save()
```

Now when query for all objects of type Category with the command:
```python
Category.objects.all()
```
Will see the newly entered Category entry.

It is also possible to examine category:
```python
new_category.id
new_category.title
new_category.created_at
```

To query for an entry by the primary key, execute following command:
```python
Category.objects.get(pk=1)
```

Or to query by other fields, for example by `title`:
```python
Category.objects.get(title='Programming')
```

To search by some field, execute following command:
```python
Category.objects.filter(title='Programming')
```
This will return the Set of Categories.

It is also possible to query fy wildcards like this:
```python
Category.objects.filter(title__contains='ing')
```
